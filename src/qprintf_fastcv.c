/**=============================================================================
Copyright (c) 2017 QUALCOMM Technologies Incorporated.
_all Rights Reserved Qualcomm Proprietary
=============================================================================**/

#include "qprintf_fastcv.h"
#include "AEEStdErr.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <rpcmem.h>

int test_main_start(int argc, char* argv[]);

int main(int argc, char* argv[])
{
    return test_main_start(argc, argv);
}

// This is a simple program to initiate a test on the DSP. 
int test_main_start(int argc, char* argv[])
{
    char *qprintf_fastcv_URI_Domain;
    remote_handle64 handle = -1;
	qprintf_fastcv_URI_Domain = qprintf_fastcv_URI "&_dom=cdsp";
	
	int retVal = qprintf_fastcv_open(qprintf_fastcv_URI_Domain, &handle);
    
    // if CDSP is not present, try ADSP.
    if (retVal != 0)
    {
        printf("cDSP not detected on this target (error code %d), attempting to use aDSP\n", retVal);
        qprintf_fastcv_URI_Domain = qprintf_fastcv_URI "&_dom=adsp";
        retVal = qprintf_fastcv_open(qprintf_fastcv_URI_Domain, &handle);
    }
	if (-1 == handle || 0 != retVal)
    {
        printf("Failed to open a channel to cDSP or aDSP\n");
        return -1;
    }
        
    retVal = qprintf_fastcv_run(handle);
    
    if (1) {
        rpcmem_init();
        const uint32_t width = 1024;
        const uint32_t height = 1024;
        int16_t *src1 = rpcmem_alloc(RPCMEM_HEAP_ID_SYSTEM, RPCMEM_FLAG_CACHED, width * height * sizeof(int16_t));
        int16_t *src2 = rpcmem_alloc(RPCMEM_HEAP_ID_SYSTEM, RPCMEM_FLAG_CACHED, width * height * sizeof(int16_t));
        int16_t *dst = rpcmem_alloc(RPCMEM_HEAP_ID_SYSTEM, RPCMEM_FLAG_CACHED, width * height * sizeof(int16_t));
        if (NULL == src1 || NULL == src2 || NULL == dst) {
            printf("malloc memory failed\n");
            return -1;
        }
        
        for (uint32_t i = 0; i < width * height; ++i) {
            src1[i] = 1;
            src2[i] = 2;
        }
        memset(dst, 0, width * height * sizeof(int16_t));
        
        AEEResult retVal = qprintf_fastcv_add_s16(handle, src1, width * height, src2, width * height, dst, width * height, width, height);
        if (retVal != AEE_SUCCESS) {
            printf("qprintf_fastcv_add_s16 return value: %d\n", (int)retVal);
        }
        
        int diffCount = 0;
        for (uint32_t i = 0; i < width * height; ++i) {
            if (dst[i] != 3) {
                ++diffCount;
                if (diffCount == 1) {
                    printf("dst value: %d\n", (int)dst[i]);
                }
            }
        }
        if (diffCount == 0) {
            printf("fastcv OK\n");
        } else {
            printf("fastcv failed, diffCount: %d\n", diffCount);
        }
        rpcmem_free(src1);
        rpcmem_free(src2);
        rpcmem_free(dst);
        rpcmem_deinit();
    }
	
    
    qprintf_fastcv_close(handle);
    handle = -1;
    
    if (retVal) 
    {
        printf("Test FAILED: %d \n", retVal);
    }
    else
    {
        printf("Test PASSED\n");
    }
    printf("See Mini-DM output for more details\n");

    return retVal;
}

